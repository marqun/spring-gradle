package com.shamgod.springgradle.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author linchutao
 * @date 2021/11/13 10:59
 */
@RestController
public class HelloController {

    @GetMapping("/ping")
    public String ping() {
		System.out.println("BB");
        return "pong";
    }
}
